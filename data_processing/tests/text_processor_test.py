from ..scrap.text_processor.winiary_processor import WiniaryProcessor
import pytest


@pytest.fixture
def processor() -> WiniaryProcessor:
    return WiniaryProcessor()


@pytest.fixture
def prepare_raw_ingredient_list():
    return (
        [
            (
                "mielona wieprzowina (np. łopatka)          ",
                "              (400 g)                           ",
            ),
            (
                "                  mielona wołowina         ",
                "                 (400   g)                      ",
            ),
            (
                "             Kasza gryczana biała niepalona",
                "                  (1 szklanka)                  ",
            ),
        ],
        [
            ("mielona wieprzowina (np. łopatka)", "400 g"),
            ("mielona wołowina", "400 g"),
            ("Kasza gryczana biała niepalona", "1 szklanka"),
        ],
    )


def test_processing_ingredients(prepare_raw_ingredient_list, processor):
    raw = prepare_raw_ingredient_list[0]
    expected = prepare_raw_ingredient_list[1]
    result = processor.process_ingredients(raw)

    for e, r in zip(expected, result):
        assert e == r
