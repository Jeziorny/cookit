from ..scrap.base_scrapper import BaseScrapper, NoSources, IncorrectChunkSize
import pytest

FILENAME = "dummy"


@pytest.fixture
def scrapper():
    return BaseScrapper()


def test_raising_no_sources(scrapper):
    with pytest.raises(NoSources):
        scrapper.scrap(FILENAME)


def test_raising_incorrect_chunk_size(scrapper):
    scrapper.set_sources([])
    with pytest.raises(IncorrectChunkSize):
        scrapper.scrap(FILENAME, chunk_size=0)
