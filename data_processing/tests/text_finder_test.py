from ..scrap.finder.winiary_finder import WiniaryFinder
from bs4 import BeautifulSoup
import pytest
import os

SAMPLE_NAME = "sample.html"
EXPECTED_NUMBER_OF_STEPS = 3
EXPECTED_PREP_TIME = "30 min."
EXPECTED_PORTION_NUMBER = "6"
EXPECTED_INGREDIENTS_NUMBER = 9
EXPECTED_TITLE = "Gołąbki z mięsem i kaszą gryczaną"

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


@pytest.fixture
def soup():
    return BeautifulSoup(
        open(__location__ + "\\" + SAMPLE_NAME, encoding="utf8"), "html.parser"
    )


@pytest.fixture
def finder():
    return WiniaryFinder()


def test_finding_description(finder: WiniaryFinder, soup: BeautifulSoup):
    assert EXPECTED_NUMBER_OF_STEPS == len(finder.find_description(soup))


def test_finding_prep_time(finder: WiniaryFinder, soup: BeautifulSoup):
    assert EXPECTED_PREP_TIME == finder.find_prep_time(soup)


def test_finding_portion_number(finder: WiniaryFinder, soup: BeautifulSoup):
    assert EXPECTED_PORTION_NUMBER == finder.find_portion_num(soup)


def test_finding_ingredients(finder: WiniaryFinder, soup: BeautifulSoup):
    assert EXPECTED_INGREDIENTS_NUMBER == len(finder.find_ingredients(soup))


def test_finding_title(finder: WiniaryFinder, soup: BeautifulSoup):
    assert EXPECTED_TITLE == finder.find_title(soup)
