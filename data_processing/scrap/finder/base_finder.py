from abc import ABC, abstractmethod


class BaseTextFinder(ABC):
    """
    Responsibility of this class is to find smalles pieces of page,
    which contain specified part of recipe
    """

    @abstractmethod
    def find_description(self, web_page) -> list:
        """
        :param
            web_page: dumped web page. For default implementation type(web_page) = BeautifulSoup
        :return:
            list of steps in meal preparing
        """
        pass

    @abstractmethod
    def find_prep_time(self, web_page) -> str:
        pass

    @abstractmethod
    def find_portion_num(self, web_page) -> str:
        pass

    @abstractmethod
    def find_ingredients(self, web_page) -> list:
        """

        :param web_page:
             web_page: dumped web page. For default implementation type(web_page) = BeautifulSoup
        :return:
            list((ingredients_name, ingredients_quantity))
        """
        pass

    @abstractmethod
    def find_title(self, web_page) -> str:
        pass
