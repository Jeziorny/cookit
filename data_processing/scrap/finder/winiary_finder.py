from .base_finder import BaseTextFinder
import re


class WiniaryFinder(BaseTextFinder):
    def find_description(self, web_page) -> list:
        return [
            d.get_text()
            for d in web_page.findAll(
                "li", attrs={"itemprop": re.compile("recipeInstructions")}
            )
        ]

    def find_prep_time(self, web_page) -> str:
        prep_time = web_page.find("span", attrs={"itemprop": re.compile("prepTime")})
        if prep_time is None or prep_time == "":
            return str(None)
        else:
            return prep_time.get_text()

    def find_portion_num(self, web_page) -> str:
        portion_number = web_page.find(
            "span", attrs={"itemprop": re.compile("recipeYield")}
        )
        if portion_number is None or portion_number == "":
            return str(None)
        else:
            return portion_number.get_text()

    def find_ingredients(self, web_page) -> list:
        ingredients_section = web_page.find(
            "div", attrs={"id": re.compile("ingredients-list")}
        )
        ing_name = [
            i.get_text()
            for i in ingredients_section.findAll(
                "a", attrs={"class": re.compile("ingredients__a")}
            )
        ]
        ing_quan = [
            i.get_text()
            for i in ingredients_section.findAll(
                "span", attrs={"class": re.compile("unit")}
            )
        ]
        return list(zip(ing_name, ing_quan))

    def find_title(self, web_page) -> str:
        title = web_page.find("meta", attrs={"property": re.compile("og:title")})
        return title["content"]
