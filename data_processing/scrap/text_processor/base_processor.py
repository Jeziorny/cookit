from abc import ABC, abstractmethod


class BaseTextProcessor(ABC):
    """
    Responsibility of this class is to prepare pieces of data
    founded by BaseFinder implementation for database.
    For more information check BaseFinder which has similar structure
    """

    @abstractmethod
    def process_description(self, raw_steps: list) -> list:
        pass

    @abstractmethod
    def process_prep_time(self, raw_prep_time: str) -> str:
        pass

    @abstractmethod
    def process_portion_num(self, raw_portion_num: str) -> str:
        pass

    @abstractmethod
    def process_ingredients(self, raw_ingredients: list) -> list:
        pass

    @abstractmethod
    def process_title(self, raw_title: str) -> str:
        pass
