from .base_processor import BaseTextProcessor
import re


class WiniaryProcessor(BaseTextProcessor):
    def process_description(self, raw_steps) -> list:
        partial = [re.sub("(\r\n +)", "", s) for s in raw_steps]
        partial = [re.sub("\xa0", " ", s) for s in partial]
        return partial

    def process_prep_time(self, raw_prep_time) -> str:
        return raw_prep_time

    def process_portion_num(self, raw_portion_num) -> str:
        return raw_portion_num

    def process_ingredients(self, raw_ingredients) -> list:
        result = []
        for n, q in raw_ingredients:
            name = re.sub(" +", " ", n).strip(" ")
            quantity = re.sub("(\s+|\r\n|\xa0|[()])", " ", q).strip("  ")
            result.append((name, quantity))
        return result

    def process_title(self, raw_title) -> str:
        return raw_title
