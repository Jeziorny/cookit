import re
import json
from pathlib import Path
from typing import List, Dict

RECIPES_DIR = Path(__file__).parent.parent / "data"
ALL_RECIPES_FILE = RECIPES_DIR / "recipes_all.json"
INGREDIENTS_FILE = RECIPES_DIR / "ingredients_map.json"
IMG_URLS_FILE = RECIPES_DIR / "file_with_img_urls.json"
ING_MAP = RECIPES_DIR / "ing_to_recp_id.json"


def join_all_recipes(recipes_dir: Path) -> List[dict]:
    """Joins all recipes (since they are split into separate files)
     into one list of recipes
     """
    recipes = []
    for file in recipes_dir.iterdir():
        if re.match(r"recipes\d.json", file.name):
            with open(str(file)) as recipe:
                recipes.extend(json.load(recipe))

    return recipes


def fix_name_key(all_recipes: List[dict]):
    """Fixes key 'name:' to 'name', in place!"""
    for recipe in all_recipes:
        recipe["name"] = recipe.pop("name:")


def clean_ing(ingredients: List[str]) -> List[str]:
    ingredients = [x.lower() for x in ingredients]
    ingredients = set(ingredients)
    ingredients = list(ingredients)
    return ingredients


def add_ingredients_labels(recipes: List[dict], ingredients: List[str]):
    """Adds ingredients labels to recipes, in place"""
    ingredients = set(ingredients)
    for recipe in recipes:
        recipe["labels"] = []
        for recp_ing in recipe["ingredients"]:
            recp_ing = recp_ing["name"]
            for all_ing in ingredients:
                if all_ing in recp_ing.lower():
                    recipe["labels"].append(all_ing)


def add_id(recipes: List[dict]):
    """Adds id key to recipes, in place"""
    for i, r in enumerate(recipes, 1):
        r["id"] = i


def add_img_url(recipes: List[dict], img_urls: List[str]):
    """Adds img url to recipes, in place"""
    for recipe, img_url in zip(reversed(recipes), reversed(img_urls)):
        recipe["img"] = img_url


def match_recipe_with_img_url(recipes: List[dict], img_urls: List[str]):
    for recipe in recipes:
        recipe["img"] = ""
        name = recipe["name"]
        name = name.lower()
        name = name.strip()
        name = name.replace(" ", "-")
        name = re.sub("[ąęćźżłóń]", ".", name)
        for img in img_urls:
            if re.search(name, img):
                recipe["img"] = img


def remove_recipes_without_img(recipes: List[dict]) -> List[dict]:
    return [r for r in recipes if r["img"]]


def ingredients_to_recipe_id(
    ingredients: List[str], recipes: List[dict]
) -> Dict[str, list]:
    map_ = {ing: [] for ing in ingredients}
    for ing in map_:
        for recipe in recipes:
            if ing in recipe["labels"]:
                map_[ing].append(recipe["id"])

    map_ = {k: v for k, v in map_.items() if len(v)}
    return map_


if __name__ == "__main__":
    all_recipes = join_all_recipes(RECIPES_DIR)
    with open(INGREDIENTS_FILE) as file:
        ingredients = json.load(file)

    with open(IMG_URLS_FILE) as file:
        img_urls = json.load(file)

    ingredients = clean_ing(ingredients)
    fix_name_key(all_recipes)
    add_ingredients_labels(all_recipes, ingredients)
    add_id(all_recipes)

    match_recipe_with_img_url(all_recipes, img_urls)
    all_recipes = remove_recipes_without_img(all_recipes)

    ing_map = ingredients_to_recipe_id(ingredients, all_recipes)

    with open(ALL_RECIPES_FILE, "w", encoding="utf-8") as file:
        json.dump(all_recipes, file)

    with open(ING_MAP, "w") as file:
        json.dump(ing_map, file)
