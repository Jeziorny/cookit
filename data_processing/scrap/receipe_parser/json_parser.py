from .base_parser import BaseParser
import json


class JSONParser(BaseParser):
    def parse(self, recipes: list) -> list:
        return json.dumps(recipes, ensure_ascii=False)
