from urllib.request import urlopen
from bs4 import BeautifulSoup

from finder.base_finder import BaseTextFinder
from text_processor.base_processor import BaseTextProcessor
from receipe_parser.base_parser import BaseParser


class NoSources(Exception):
    pass


class IncorrectChunkSize(Exception):
    pass


class BaseScrapper:
    """
    Class delivers srapping functionality.
    Mandatory parameters:
    - finding strategy: class intance, which inherits from BaseTextFinder
    - processing strategy: class instance, which inherits from BaseTextProcessor
    - parsing strategy: class instance, which inherits from BaseParser.
      For now implementation delivers parsing to JSON format with JSONParser
    - sources_urls: list of sources for scrapping. Shape of source depends
      on implementation of finding and parsing strategy.
    """

    def __init__(self):
        self.sources_urls = None
        self.finding_strategy = None
        self.parsing_strategy = None
        self.processing_strategy = None
        self.ingredients_tracking = False

    @staticmethod
    def _ing_to_dict(ing: list) -> list:
        result = []
        for i in ing:
            result.append({"name": i[0], "q": i[1]})
        return result

    @staticmethod
    def _to_dict(name, desc, prep, portions, ing):
        return {
            "name:": name,
            "description": desc,
            "prep_time": prep,
            "portions": portions,
            "ingredients": BaseScrapper._ing_to_dict(ing),
        }

    @staticmethod
    def _write_to_file(recipes: list, out_file):
        try:
            with open(out_file, "w", encoding="UTF-8") as file:
                file.writelines(recipes)
        except IOError:
            print("IOError raise")

    def set_sources(self, pages: list):
        """sets sources for scrapping"""
        self.sources_urls = pages

    def set_finding_strategy(self, finding_strategy: BaseTextFinder):
        """sets finding strategy, required interface: BaseTextFinder"""
        self.finding_strategy = finding_strategy

    def set_processing_strategy(self, processing_strategy: BaseTextProcessor):
        """sets processing strategy, required interface: BaseTextProcessor"""
        self.processing_strategy = processing_strategy

    def set_parsing_strategy(self, parsing_strategy: BaseParser):
        """sets parsing strategy, required interface: BaseParser"""
        self.parsing_strategy = parsing_strategy

    def scrap(self, out_file, *, chunk_size=1000, ingredients_tracking_file=None):
        """
        Call this function to scrap information from delivered URLs
        :param out_file:
            file containing result of scrapping.
            If chunk_size is less than number of all recipes (len(sources)), then
            multiple files with shape of out_file[1-N].json are on output.
        :param chunk_size:
            specifies number of recipes in single resulting file, default chunk_size=1000
        :param ingredients_tracking_file:
            if specified, there will be file with all met ingredients on output with
            ingredients_tracking_file name. In other case there will not be recorder
            all met ingredients.
        :return:
            None
        :raises:
            :exception NoSources if no sources were delivered,
            :exception IncorrectChunkSize if chunk_size < 2.
        """

        if self.sources_urls is None:
            raise NoSources(
                "No sources were delivered. Use set_sources(pages: list) first."
            )

        if chunk_size > 1:
            self.sources_urls = [
                self.sources_urls[i : i + chunk_size]
                for i in range(0, len(self.sources_urls), chunk_size)
            ]
        else:
            raise IncorrectChunkSize(
                "size of chunk must be greater than one. Current chunk: {}".format(
                    chunk_size
                )
            )

        ingredients_set = set()

        for chunk_idx, chunk in enumerate(self.sources_urls):
            part_result = []
            for url_idx, url in enumerate(chunk):
                soup = BeautifulSoup(urlopen(url).read().decode("utf-8"), "html.parser")
                title = self.processing_strategy.process_title(
                    self.finding_strategy.find_title(soup)
                )
                description = self.processing_strategy.process_description(
                    self.finding_strategy.find_description(soup)
                )
                prep_time = self.processing_strategy.process_prep_time(
                    self.finding_strategy.find_prep_time(soup)
                )
                portion_number = self.processing_strategy.process_portion_num(
                    self.finding_strategy.find_portion_num(soup)
                )
                ingredients = self.processing_strategy.process_ingredients(
                    self.finding_strategy.find_ingredients(soup)
                )
                part_result.append(
                    self._to_dict(
                        title, description, prep_time, portion_number, ingredients
                    )
                )
                if ingredients_tracking_file is not None:
                    ingredients_set = ingredients_set | set(i[0] for i in ingredients)
                if url_idx % 10 == 0:
                    print("{} scrapped recipes".format(url_idx))
            parsed_recipes = self.parsing_strategy.parse(part_result)
            BaseScrapper._write_to_file(
                parsed_recipes, out_file + str(chunk_idx) + ".json"
            )
            print("chunk {} scrapped".format(chunk_idx))
        if ingredients_set:
            ingredients_list = self.parsing_strategy.parse(
                {"ingredients": list(ingredients_set)}
            )
            BaseScrapper._write_to_file(
                ingredients_list, ingredients_tracking_file + ".json"
            )
