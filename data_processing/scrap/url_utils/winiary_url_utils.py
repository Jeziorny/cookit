from urllib.request import urlopen
from bs4 import BeautifulSoup

import os
import re
import json

PAGES_WITH_RECIPES_URL_PATTERN = (
    "https://www.winiary.pl/szukaj.aspx/przepisy/{}?from=winiary"
)
PAGES_NUMBER = 25

"""
Helper script for gathering links with recipes.
Not official part of project.
"""


def write_to_file(urls: list):
    file_with_urls = "../data/file_with_urls.json"
    os.makedirs(os.path.dirname(file_with_urls), exist_ok=True)
    with open(file_with_urls, "w") as f:
        f.writelines(urls)


if __name__ == "__main__":
    result = []
    for pages in range(PAGES_NUMBER):
        current_url = PAGES_WITH_RECIPES_URL_PATTERN.format(pages)
        html = urlopen(current_url).read().decode("utf-8")
        soup = BeautifulSoup(html, "html.parser")
        for chunk in soup.findAll("a", attrs={"itemprop": re.compile("name")}):
            result.append(chunk.get("href"))

        if pages % 5 == 0:
            print("{} pages done".format(pages))

    write_to_file(json.dumps(result))
