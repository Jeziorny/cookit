import json

from base_scrapper import BaseScrapper
from finder.winiary_finder import WiniaryFinder
from receipe_parser.json_parser import JSONParser
from text_processor.winiary_processor import WiniaryProcessor


def prepare_sources(f_name: str) -> list:
    in_file = open(f_name)
    return json.load(in_file)


if __name__ == "__main__":
    winiary_scrapper = BaseScrapper()

    winiary_scrapper.set_finding_strategy(WiniaryFinder())
    winiary_scrapper.set_parsing_strategy(JSONParser())
    winiary_scrapper.set_processing_strategy(WiniaryProcessor())

    winiary_scrapper.ingredients_tracking = True

    sources = prepare_sources("data/file_with_urls.json")

    winiary_scrapper.set_sources(sources)

    winiary_scrapper.scrap(
        "temp.json", ingredients_tracking_file="temp_ingredients.json"
    )
