import React, {useState, useEffect} from "react";
import "./Favourites.scss";
import NotLogged from "../NotLogged/NotLogged";
import {subscribeToAuthChange} from "../../utils/authUtils";
import RecipesList from "../RecipesList/RecipesList";
import firebase from "firebase";
import Loader from "../Loader/Loader";
import {Link} from "react-router-dom";

export default function Favourites() {
  const [user, setUser] = useState(null);
  const [favourites, setFavourites] = useState(null);
  const [availableIngredients, setAvailableIngredients] = useState(null)

  subscribeToAuthChange(
      signedIn => {
        setUser(signedIn);
      },
      signedOut => {
        setUser(signedOut);
      }
  );

  const fetchFavourites = async () => {
    var favourites_list = [];
    await firebase
        .database()
        .ref("favourites/" + user.uid)
        .on("value", snapshot => {
          snapshot.forEach(recipe => {
            favourites_list.push(recipe.val().recipeId);
          });
          setFavourites(favourites_list);
        });
  };

  const fetchUserAvailableIngredients = async () => {
    let tempUserIngredients = [];
    await firebase
      .database()
      .ref("user-ingredients/" + user.uid)
      .on("value", snapshot => {
        snapshot.forEach(userIngredient => {
          tempUserIngredients.push(userIngredient.val());
        });
        setAvailableIngredients(tempUserIngredients);
      });
  }

  useEffect(() => {
    if (user) {
      fetchFavourites();
      fetchUserAvailableIngredients();
    }
  }, [user]);

  const renderLoader = <Loader/>;

  const logged = () => {
    return favourites ? checkFavourites() : renderLoader;
  };

  const checkFavourites = () => {
    return favourites.length > 0 ? renderFavourites(favourites) : noRecipes();
  };

  const noRecipes = () => {
    return (
        <div className="NotLogged">
          <h1>Cześć!</h1>
          <h3>
            Nie dodałeś jeszcze żadnego przepisu do ulubionych.
          </h3>
        </div>
    );
  };

  const renderFavourites = favourites => {
    return (
        <div>
          <h1>Ulubione przepisy</h1>
          <RecipesList
              availableIngredients={availableIngredients}
              recipesIDList={favourites}
              favouriteRecipes={favourites}
          />
        </div>
    );
  };

  const notLogged = <NotLogged/>;

  const content = user ? logged() : notLogged;

  return <div className="Favourites">{content}</div>;
}
