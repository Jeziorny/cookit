import React, {useEffect, useState} from 'react';
import './ShoppingScreen.scss';
import NotLogged from "../NotLogged/NotLogged";
import ShoppingList from "../ShoppingList/ShoppingList";
import {subscribeToAuthChange} from "../../utils/authUtils";
import firebase from "firebase";
import Loader from "../Loader/Loader";

const ShoppingScreen = () => {

  const [user, setUser] = useState(null);
  const [shoppingIng, setShoppingIng] = useState(null);

  const sub = subscribeToAuthChange(signedIn => {
    setUser(signedIn);
  }, signedOut => {
    setUser(signedOut);
  });

  const removeFromShoppingList = boughtProd => {
    boughtProd.forEach(prod => {
      firebase
          .database()
          .ref("/shopping-lists/" + user.uid)
          .once("value")
          .then(snap => {
            snap.forEach(child => {
              if (child.val().ingredient === prod) {
                child.ref.remove().then(() => {
                });
              }
            });
          });
      setShoppingIng(null);
      fetchShoppingList()
    });
  };

  const logged = (
      <div className="logged">
        <h1>Twoja lista zakupów</h1>
        {shoppingIng ? <ShoppingList ingredients={shoppingIng} removeFromShoppingList={removeFromShoppingList}/> : <Loader />}
      </div>
  );

  const notLogged = (
      <NotLogged/>
  );

  const content = user ? logged : notLogged;

  useEffect(() => {
    if (user) {
      fetchShoppingList();
    }
  }, [user]);

  const fetchShoppingList = async () => {
    let ingredient_list = [];
    await firebase
        .database()
        .ref("shopping-lists/" + user.uid)
        .on("value", snapshot => {
          snapshot.forEach(ingredient => {
            ingredient_list.push(ingredient.val().ingredient);
          });
          setShoppingIng(ingredient_list);
        });
  };

  return (
      <div className="ShoppingScreen">
        {content}
      </div>
  );
};

export default ShoppingScreen;
