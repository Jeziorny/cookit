import React, { useEffect, useState } from "react";
import RecipesList from "../RecipesList/RecipesList.js";
import { subscribeToAuthChange } from "../../utils/authUtils";
import firebase from "firebase";
import Loader from "../Loader/Loader";
import { Link } from "react-router-dom";
import { firebasePaths, sessionStoragePaths } from "../../config/endPoints";
import "./RecipeFinder.scss";

export default function RecipeFinder() {
  const [user, setUser] = useState(null);
  const [favourites, setFavourites] = useState(null);
  const [availableIngredients, setAvailableIngredients] = useState(null);
  const [usersIngredientsAppearIn, setUsersIngredientsAppearIn] = useState(
    null
  );

  subscribeToAuthChange(
    signedIn => {
      setUser(signedIn);
    },
    signedOut => {
      setUser(signedOut);
    }
  );

  // Download favourties recipes based on user UID
  const fetchFavourites = async () => {
    var favourites_list = [];
    await firebase
      .database()
      .ref("favourites/" + user.uid)
      .on("value", snapshot => {
        snapshot.forEach(recipe => {
          favourites_list.push(recipe.val().recipeId);
        });
        setFavourites(favourites_list);
      });
  };

  // Download user available ingredients
  const fetchIngredientsWithRelatedRecipes = async () => {
    let tempUserIngredients = [];
    await firebase
      .database()
      .ref("user-ingredients/" + user.uid)
      .on("value", snapshot => {
        snapshot.forEach(userIngredient => {
          tempUserIngredients.push(userIngredient.val());
        });
        setAvailableIngredients(tempUserIngredients);
      });
  };

  // Download recipes where user's ingredients are used
  const fetchUserIngredientsAppearIn = async () => {
    let tempAvailableRecipes = [];
    await firebase
      .database()
      .ref("ing_to_recp_id")
      .on("value", snapshot => {
        snapshot.forEach(ingredient => {
          if (availableIngredients.includes(ingredient.key)) {
            ingredient.forEach(recipe => {
              tempAvailableRecipes.push(recipe.val());
            });
          }
        });
        setUsersIngredientsAppearIn(tempAvailableRecipes);
      });
  };

  useEffect(() => {
    if (availableIngredients) {
      fetchUserIngredientsAppearIn();
    }
  }, [availableIngredients]);

  useEffect(() => {
    if (user) {
      fetchFavourites();
      fetchIngredientsWithRelatedRecipes();
    } else {
      setAvailableIngredients(
        getAnonymousIngredients(
          JSON.parse(
            sessionStorage.getItem(sessionStoragePaths.userIngredients)
          )
        )
      );
    }
  }, [user]);

  const noRecipes = () => {
    return (
      <div className="NotLogged">
        <h1>Cześć!</h1>
        <h3>
          Nie posiadasz jeszcze żadnych składników w lodówce!
          <br /> Dodaj składniki, aby zacząć przeglądać przepisy!
        </h3>
        <div className="wrapCenter">
          <Link to="/fridge">
            <button className="btn btn-info mt-4 mobileButton">Lodówka</button>
          </Link>
        </div>
      </div>
    );
  };

  const renderLoader = <Loader />;

  const renderList = favRecipes => {
    return (
        <div>
          <h1>Dostępne przepisy</h1>
          <RecipesList
              availableIngredients={availableIngredients}
              recipesIDList={usersIngredientsAppearIn}
              favouriteRecipes={favRecipes}
          />
        </div>
    );
  };

  const displayRecipes = favRecipes => {
    return usersIngredientsAppearIn.length > 0
      ? renderList(favRecipes)
      : noRecipes();
  };

  const getAnonymousIngredients = object => {
    let avIngredients = [];
    for (var key in object) {
      avIngredients.push(object[key]);
    }
    return avIngredients;
  };

  const renderAnonymousList =
    availableIngredients && usersIngredientsAppearIn
      ? displayRecipes([])
      : renderLoader;

  const renderUserList =
    favourites && availableIngredients && usersIngredientsAppearIn
      ? displayRecipes(favourites)
      : renderLoader;

  return (
    <div className="RecipeFinder">
      {user ? renderUserList : renderAnonymousList}
    </div>
  );
}
