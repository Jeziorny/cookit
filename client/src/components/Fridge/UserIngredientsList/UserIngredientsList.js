import React from 'react';
import './UserIngredientsList.scss';
import UserIngredientsListElement from "./UserIngredientsListElement/UserIngredientsListElement";


export default function UserIngredientsList({data, loggedIn, refreshCallback}) {
  const listElements = data ? Object.entries(data).map(([idx, value]) => {
    return <UserIngredientsListElement key={idx} index={idx} ingredient={value} loggedIn={loggedIn}
                                       refreshCallback={refreshCallback}/>
  }) : null;

  return (
      <div className="UserIngredientsList">
        <div className="bottom-border"/>
        {listElements}
      </div>
  );
}