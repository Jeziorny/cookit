import React from 'react';
import './UserIngredientsListElement.scss';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import {firebasePaths, sessionStoragePaths} from "../../../../config/endPoints";
import {getDatabaseRef, getUserId} from "../../../../utils/firebaseUtils";
import {LoggingStatus} from "../../Fridge";

export default function UserIngredientsListElement({index, ingredient, loggedIn, refreshCallback}) {

  const deleteMe = () => {
    console.log(loggedIn)
    if(loggedIn === LoggingStatus.IN) {
      getDatabaseRef(firebasePaths.userIngredients)
          .child(getUserId())
          .child(index)
          .remove();
    } else {
      const localList = JSON.parse(sessionStorage.getItem(sessionStoragePaths.userIngredients));
      delete localList[index];
      sessionStorage.setItem(sessionStoragePaths.userIngredients, JSON.stringify(localList));
      refreshCallback();
    }
  };

  return (
      <div className="UserIngredientsListElement">
        <div className="ingredient-card">
          <div className="ingredient-content">{ingredient}</div>
          <IconButton className="delete-btn" aria-label="delete" onClick={deleteMe}>
            <DeleteIcon/>
          </IconButton>
        </div>
        <div className="fridge-shelf"/>
      </div>
  );
}