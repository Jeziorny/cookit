import React from 'react';
import './Autocompletion.scss';
import {Autocomplete} from "@material-ui/lab";
import TextField from "@material-ui/core/TextField";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {firebasePaths, sessionStoragePaths} from "../../../config/endPoints";
import {getDatabaseRef, getUserId} from "../../../utils/firebaseUtils";
import {LoggingStatus} from "../Fridge";
import * as pushid from "pushid";

export default class Autocompletion extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    };
  }

  addIngredient = () => {
    const value = this.state.selectedOption;

    if (value) {
      if (this.props.loggedIn === LoggingStatus.IN) {
        const id = getUserId();
        if (id) {
          const ref = getDatabaseRef(`${firebasePaths.userIngredients}${id}`).push();
          ref.set(value);
          this.setState({selectedOption: null});
        }
      } else {
        const ingredients = JSON.parse(sessionStorage.getItem(sessionStoragePaths.userIngredients));
        ingredients[pushid()] = value;
        sessionStorage.setItem(sessionStoragePaths.userIngredients, JSON.stringify(ingredients));
        this.props.refreshCallback();
        this.setState({selectedOption: null})
      }
    }
  };

  filterIngredientsOptions = (options, { inputValue }) => {
    return options
        .filter(option => option.includes(inputValue))
        .filter(option => !this.props.currentListValues.includes(option))
        .slice(0, this.props.numberOfOptions);
  };

  render() {
    return (
        <div className="Autocompletion">
          <Autocomplete
              disableClearable
              clearOnEscape
              options={this.props.options}
              filterOptions={this.filterIngredientsOptions}
              value={this.state.selectedOption}
              onChange={(event, value) => this.setState({selectedOption: value})}
              renderInput={params => (
                  <TextField
                      {...params}
                      label="Nowy składnik"
                      variant="outlined"
                      fullWidth
                  />
              )}
          />
          <Fab color="primary" aria-label="add" onClick={this.addIngredient}>
            <AddIcon/>
          </Fab>
        </div>
    );
  }
}