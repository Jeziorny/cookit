import React from 'react';
import './Fridge.scss';
import UserIngredientsList from "./UserIngredientsList/UserIngredientsList";
import fridgeRight from "../../assets/images/fridge-right.svg";
import Autocompletion from "./Autocompletion/Autocompletion";
import {subscribeToAuthChange} from "../../utils/authUtils";
import {firebasePaths, sessionStoragePaths} from "../../config/endPoints";
import {getDatabaseRef, getUserId} from "../../utils/firebaseUtils";

export const LoggingStatus = {
  IN: 1, // logged in
  OUT: 2 // logged out
};


export default class Fridge extends React.Component {
  constructor(props) {
    super(props);
    this.unsubscribeFromAuthChange = null;
    this.state = {
      existingIngredientsList: [],
      userIngredientsList: [],
      selectedOption: null,
      loggedIn: LoggingStatus.OUT
    };
  }

  componentDidMount() {
    this.unsubscribeFromAuthChange = subscribeToAuthChange(() => {
      this.setState({loggedIn: LoggingStatus.IN});
      this.fetchUserIngredientsList();
    }, () => {
      this.setState({loggedIn: LoggingStatus.OUT});
      this.initSessionStorage();
      this.fetchUserIngredientsList();
    });
    this.fetchExistingIngredientsList();
  }

  componentWillUnmount() {
    this.unsubscribeFromAuthChange();
  }

  fetchExistingIngredientsList = () => {
    const ref = getDatabaseRef(firebasePaths.ingredientsWithRelatedRecipes);
    ref.once('value').then(snapshot => {
      const filteredEntries = Object.entries(snapshot.val())
          .filter(([key, val]) => val && val.length > 0);
      const list = filteredEntries.map(arr => arr[0]);
      this.setState({existingIngredientsList: list});
    });
  };

  fetchUserIngredientsList = () => {
    if (this.state.loggedIn === LoggingStatus.IN) {
      const id = getUserId();
      if (id) {
        const ref = getDatabaseRef(`${firebasePaths.userIngredients}${id}`);
        ref.on('value', snapshot => {
          this.setState({userIngredientsList: snapshot.val()})
        });
      }
    } else {
      this.getUserIngredientsListFromSessionStorage();
    }
  };

  getUserIngredientsListFromSessionStorage = () => {
    const localList = JSON.parse(sessionStorage.getItem(sessionStoragePaths.userIngredients));
    this.setState({userIngredientsList: localList});
  };

  initSessionStorage() {
    if (!sessionStorage.getItem(sessionStoragePaths.userIngredients)) {
      sessionStorage.setItem(sessionStoragePaths.userIngredients, JSON.stringify({}));
    }
  }

  render() {
    return (
        <div className="Fridge">
          <Autocompletion loggedIn={this.state.loggedIn}
                          options={this.state.existingIngredientsList}
                          refreshCallback={this.getUserIngredientsListFromSessionStorage}
                          currentListValues={this.state.userIngredientsList ? Object.values(this.state.userIngredientsList) : []}
                          numberOfOptions={5}/>
          <div className="fridge-graphic">
            <div className="front">
              <div className="main">
                <UserIngredientsList refreshCallback={this.getUserIngredientsListFromSessionStorage}
                                     data={this.state.userIngredientsList} loggedIn={this.state.loggedIn}/>
              </div>
              <div className="bottom"/>
            </div>
            <img src={fridgeRight} alt="Visual part of Fridge"/>
          </div>
        </div>
    );
  }

}
