import "./Logo.scss";
import React from "react";

const Logo = () => {
  return (
    <div className="Logo">
      <span className="text-monospace">
        Cook<span style={{ color: "green" }}>.it</span>
      </span>
    </div>
  );
};

export default Logo;
