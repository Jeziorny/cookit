import React, {useEffect, useState} from "react";
import "./IngredientsList.scss";
import firebase from "firebase";

function IngredientsList({ availableIngredients, ingredients, user }) {
  const tempIngredients = [];

  const [shoppingIng, setShoppingIng] = useState(null);
  const [success, setSuccess] = useState(false);

  const renderCheckboxes = () => {
    ingredients.forEach(ingredient => {
      var res = ingredient.name.split(" ");
      var counter = 0

      if(availableIngredients.includes(ingredient.name)){
        counter=counter +1 ;
      }
      res.forEach((ing)=>{
        if (availableIngredients.includes(ing.toLowerCase())) {
          counter = counter + 1
        }
      })
      if(counter > 0){
        tempIngredients.push({
          q: ingredient.q,
          name: ingredient.name,
          available: true
        });
      } else {
        tempIngredients.push({
          q: ingredient.q,
          name: ingredient.name,
          available: false
        });
      }
      counter = 0;
    });
  };

  useEffect(() => {
    if (shoppingIng) {
      saveToShoppingList();
    }
  }, [shoppingIng]);

  const renderIngredients = () => {
    renderCheckboxes();
    tempIngredients.sort((x, y) =>
      x.available === y.available ? 0 : x.available ? 1 : -1
    );

    return (
      <ul>
        {tempIngredients.map((ingredient,key) => {
          let ingColor = "black";
          if (!ingredient.available) {
            ingColor = "red";
          }
          return (
            <li key={key} >
              <p style={{ color: ingColor }}>{ingredient.name}</p> <p style={{fontWeight: "bolder"}}>{ingredient.q}</p>
            </li>
          );
        })}
      </ul>
    );
  };

  const fetchShoppingList = async () => {
    let ingredient_list = [];
    await firebase
        .database()
        .ref("shopping-lists/" + user.uid)
        .on("value", snapshot => {
          snapshot.forEach(ingredient => {
            ingredient_list.push(ingredient.val().ingredient);
          });
          setShoppingIng(ingredient_list);
        });
  };

  const saveToShoppingList = () => {
    tempIngredients.forEach(ingredient => {
      if (!ingredient.available && !shoppingIng.includes(ingredient.name)) {
        firebase
            .database()
            .ref("/shopping-lists/" + user.uid)
            .push({ingredient: ingredient.name});
      }
    });
  };

  const renderSuccess =() => {
    return (
        <div className="alert alert-success alert-dismissible fade show">
          <button type="button" className="close" data-dismiss="alert" onClick={() => {setSuccess(false)}}>&times;</button>
          Dodano do listy zakupów.
        </div>
    )
  };

  return (
      <div className="IngredientList">
        <h5>Składniki:</h5>
        {renderIngredients()}
        {user && <button
            className="btn btn-success mb-4"
            onClick={() => {fetchShoppingList(); setSuccess(true)}}
        >
          Dodaj brakujące do listy zakupów
        </button>}
        {success ? renderSuccess() : null}
      </div>
  );
}

IngredientsList.propTypes = {};

export default IngredientsList;
