import React from "react";
import { Link } from "react-router-dom";

const NotLogged = () => {
  return (
      <div className="NotLogged">
        <h1>Cześć!</h1>
        <h3>Aby korzystać ze wszystkich opcji naszej aplikacji załóż konto tutaj!</h3>
        <Link to='/auth'>
          <button className="btn btn-success mt-4 mb-5 mobileButton">
            Załóż konto / Zaloguj się
          </button>
        </Link>
        <h3>Możesz także przejść do <b>Lodówki</b> i założyć konto później.</h3>
        <h3>Smacznego!</h3>
        <Link to='/fridge'>
          <button className="btn btn-info mt-4 mobileButton">
            Lodówka
          </button>
        </Link>
      </div>
  )

};

export default NotLogged;
