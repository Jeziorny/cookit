import RecipesListElement from "./RecipesListElement/RecipesListElement";
import "./RecipesList.scss";
import StackGrid from "react-stack-grid";
import React, { useState, useEffect } from "react";
import firebase from "firebase";
import { Breakpoint } from "react-socks";
import Loader from "../Loader/Loader";
import { subscribeToAuthChange } from "../../utils/authUtils";
import * as _ from "lodash";

export default function RecipesList({
  availableIngredients,
  recipesIDList,
  favouriteRecipes
}) {
  const [recipesList, setRecipesList] = useState(null);
  const [user, setUser] = useState(null);
  

  subscribeToAuthChange(
    signedIn => {
      setUser(signedIn);
    },
    signedOut => {
      setUser(signedOut);
    }
  );

  const fetchRecipesList = async () => {
    const recipesList_from_db = await firebase.database().ref("recipes");
    recipesList_from_db.on("value", snapshot => {
      const properRecipes = [];
      snapshot.forEach(recipe => {
        if (recipesIDList.includes(recipe.val().id_)) {
          if (favouriteRecipes.includes(recipe.val().id_)) {
            properRecipes.push({
              id: recipe.val().id_,
              name: recipe.val().name,
              ingredients: recipe.val().ingredients,
              prep_time: recipe.val().prep_time,
              portions: recipe.val().portions,
              img: recipe.val().img,
              favourite: true
            });
          } else {
            properRecipes.push({
              id: recipe.val().id_,
              name: recipe.val().name,
              ingredients: recipe.val().ingredients,
              prep_time: recipe.val().prep_time,
              portions: recipe.val().portions,
              img: recipe.val().img,
              favourite: false
            });
          }
        }
      });
      setRecipesList(properRecipes);
    });
  };

  useEffect(() => {
    fetchRecipesList();
  }, []);

  const ingredientsList = (recipeIngredients, availableIngredients) => {
    const temp_missing = [];
    recipeIngredients.forEach(ingredient => {
      var res = ingredient.name.split(" ");
      var counter = 0

      if(availableIngredients.includes(ingredient.name)){
        counter=counter +1 ;
      }

      res.forEach((ing)=>{
        if (availableIngredients.includes(ing.toLowerCase())) {
          counter = counter + 1
        }
      })
      if(counter == 0){
        temp_missing.push({
          q: ingredient.q,
          name: ingredient.name,
          available: false
        });
      }
      counter = 0;
    });
    return temp_missing;
  };

  const renderLoader = () => {
    return <Loader />;
  };


  const getRecipeList = () => {
    return _.sortBy(recipesList.map((recipe, key) => {
      return (
          <RecipesListElement
              key={key}
              user={user}
              recipeId={recipe.id}
              recipeName={recipe.name}
              recipeIngredients={recipe.ingredients}
              availableIngredients={availableIngredients}
              missingIngredients={ingredientsList(recipe.ingredients, availableIngredients)}
              prep_time={recipe.prep_time}
              portions={recipe.portions}
              pictureURL={recipe.img}
              currFavourite={recipe.favourite}
          />
      );
    }), 'props.missingIngredients.length');
  };

  const renderRecipesList = () => {
    return (
      <div className="RecipesList">
        <Breakpoint small down>
          {getRecipeList()}
        </Breakpoint>
        <Breakpoint medium up>
          <StackGrid columnWidth={350}>
            {getRecipeList()}
          </StackGrid>
        </Breakpoint>
      </div>
    );
  };

  return recipesList ? renderRecipesList() : renderLoader();
}
