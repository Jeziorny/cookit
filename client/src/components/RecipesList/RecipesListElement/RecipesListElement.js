import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import "./RecipesListElement.scss";
import PreparationTime from "../../Recipe/PreparationTime/PreparationTime";
import firebase from "firebase";

import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345,
    minWidth: "350px",
    minHeight: "450px",
    textAlign: "center",
    justifyContent: "center",
    marginBottom: "20px",
    marginTop: "20px"
  },
  media: {
    paddingTop: "56.25%" // 16:9
  }
}));

export default function RecipesListElement({
  user,
  recipeId,
  recipeName,
  prep_time,
  portionAmount,
  availableIngredients,
  recipeIngredients,
  missingIngredients,
  pictureURL,
  currFavourite
}) {
  const classes = useStyles();
  const [favourite, setFavourite] = useState(currFavourite);

  const addToFavourites = () => {
    if (!favourite) {
      firebase
        .database()
        .ref("/favourites/" + user.uid)
        .push({ recipeId });
      setFavourite(true);
    } else {
      firebase
        .database()
        .ref("/favourites/" + user.uid)
        .once("value")
        .then(snap => {
          snap.forEach(child => {
            if (child.val().recipeId == recipeId) child.ref.remove();
            setFavourite(false);
          });
        });
    }
  };

  let iconColor = favourite ? "red" : "grey";

  useEffect(() => {
    iconColor = favourite ? "red" : "grey";
  }, [favourite]);
  let path = `/recipe/${recipeId}`;

  const displayAlert = () => {
    
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <IconButton
            className={classes.customHoverFocus}
            onClick={user ? addToFavourites : displayAlert}
            aria-label="add to favorites"
            style={{ color: iconColor }}
          >
            <FavoriteIcon />
          </IconButton>
        }
        title={recipeName}
        subheader={portionAmount}
        titleTypographyProps={{ variant: "subtitle2" }}
      />
      <Link to={path}>
        <CardMedia
          className={classes.media}
          image={pictureURL}
          title={recipeName}
        />
        <CardContent>
          <PreparationTime size={25} time={prep_time} />
          <Typography variant="subtitle2">
            Brakuje Ci {Object.keys(missingIngredients).length} składników
          </Typography>
        </CardContent>
      </Link>
    </Card>
  );
}
