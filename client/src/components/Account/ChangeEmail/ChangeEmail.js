import React, {useState} from "react";
import '../Account.scss';
import * as yup from "yup";
import {Formik, Form} from "formik";
import TextField from "../../TextField/TextField";
import firebase from "firebase";

const form = {
  email: ""
};

const validationSchema = yup.object({
  email: yup
      .string()
      .email("podany email jest niepoprawny")
      .required("pole email jest wymagane")
});

const onSubmit = (data, setSubmitting, setSuccess, setError) => {
  setSubmitting(true);
  firebase.auth()
      .currentUser.updateEmail(data.email).then(() => {
    setSuccess(true);
  }).catch(err => {
    console.log(err);
    setError(true);

  });
  setSubmitting(false);
};

const ChangeEmail = ({profile}) => {

  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const renderSuccess = () => {
    return (
        <div className="alert alert-success alert-dismissible fade show">
          <button type="button" className="close" data-dismiss="alert" onClick={() => {setSuccess(false)}}>&times;</button>
          <strong>Sukces!</strong> Twój adres email został zmieniony.
        </div>
    )
  };

  const renderError = () => {
    return (
        <div className="alert alert-danger alert-dismissible fade show">
          <button type="button" className="close" data-dismiss="alert" onClick={() => {setError(false)}}>&times;</button>
          <strong>Błąd!</strong> Wystąpił błąd. Spróbuj wylogować się i zalogować ponownie.
        </div>
    )
  };

  return (
      <div className="ChangeEmail">
        {success ? renderSuccess() : null}
        {error ? renderError() : null}
        <Formik
            initialValues={form}
            validationSchema={validationSchema}
            onSubmit={(data, {setSubmitting}) => {
              onSubmit(data, setSubmitting, setSuccess, setError);
            }}
        >
          {({isSubmitting}) => (
              <Form>
                <div className="textFieldWrapper">
                  <TextField
                      className="textField"
                      label={profile ? profile.email : 'Email'}
                      name="email"
                      type="email"
                  />
                </div>
                <button
                    className="btn btn-success mobileButton rightButton"
                    type="submit"
                    disabled={isSubmitting}
                >
                  Zmień
                </button>
              </Form>
          )}
        </Formik>
      </div>
  )
};

export default ChangeEmail;
