import React, {useState} from 'react';
import './Account.scss';
import useDisplayName from '../../hooks/useDisplayName';
import ChangeEmail from "./ChangeEmail/ChangeEmail";
import ChangePassword from "./ChangePassword/ChangePassword";
import NotLogged from "../NotLogged/NotLogged";
import {subscribeToAuthChange} from "../../utils/authUtils";
import { useHistory } from "react-router-dom";
import firebase from "firebase";

const Account = () => {

  const history = useHistory();

  const logout = (event) => {
    event.persist();
    if (user) {
      firebase.auth().signOut().then(() => {
        history.push('/auth');
      })
    }
  };

  const deleteUser = () => {
    if (user) {
      firebase.auth().currentUser.delete().then(() => {
        history.push('/auth');
      });
    }
  };

  const [user, setUser] = useState(null);
  const [profile, setProfile] = useState(null);
  const [firstName] = useDisplayName(profile ? profile.displayName : "");

  const sub = subscribeToAuthChange(signedIn => {
    setUser(signedIn);
    setProfile(user ? user.providerData[0] : null);
  }, signedOut => {
    setUser(signedOut);
  });

  const logged = (
      <div className="logged">
        <h1>Cześć, {firstName}!</h1>
        <h3>Zmień email</h3>
        <ChangeEmail profile={profile}/>
        <hr/>
        <h3>Zmień hasło</h3>
        <ChangePassword/>
        <hr/>
        <div className="onMobile">
          <h3>Wyloguj</h3>
          <button className="btn btn-info mobileButton mt-4" onClick={(event) => {logout(event)}}>
            Wyloguj
          </button>
          <hr/>
        </div>
        <h3>Usuń konto</h3>
        <button className="btn btn-danger mobileButton mt-4" onClick={() => {deleteUser()}}>
          Usuń konto
        </button>
      </div>
  );

  const notLogged = (
      <NotLogged/>
  );

  const content = user ? logged : notLogged;

  return (
      <div className="Account">
        {content}
      </div>
  );
};

export default Account;
