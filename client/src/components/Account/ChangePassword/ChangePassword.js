import React, {useState} from "react";
import '../Account.scss';
import * as yup from "yup";
import {Formik, Form} from "formik";
import TextField from "../../TextField/TextField";
import firebase from "firebase";

const form = {
  password: "",
  rePassword: ""
};

const validationSchema = yup.object({
  password: yup
      .string()
      .min(6, "hasło zbyt krótkie, minimum 6 znaków")
      .required("pole hasło jest wymagane"),
  rePassword: yup
      .string()
      .required("pole potwierdź hasło jest wymagane")
      .oneOf([yup.ref("password")], "podane hasła różnią się")
});

const onSubmit = (data, setSubmitting, setSuccess, setError) => {
  setSubmitting(true);
  firebase.auth()
      .currentUser.updatePassword(data.password).then(() => {
    setSuccess(true);
  }).catch(err => {
    setError(true);
  });
  setSubmitting(false);
};

const ChangePassword = () => {

  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const renderSuccess = () => {
    return (
        <div className="alert alert-success alert-dismissible fade show">
          <button type="button" className="close" data-dismiss="alert" onClick={() => {setSuccess(false)}}>&times;</button>
          <strong>Sukces!</strong> Twoje hasło zostało zmienione.
        </div>
    )
  };

  const renderError = () => {
    return (
        <div className="alert alert-danger alert-dismissible fade show">
          <button type="button" className="close" data-dismiss="alert" onClick={() => {setError(false)}}>&times;</button>
          <strong>Błąd!</strong> Wystąpił błąd. Spróbuj wylogować się i zalogować ponownie.
        </div>
    )
  };

  return (
      <div className="ChangePassword">
        {success ? renderSuccess() : null}
        {error ? renderError() : null}
        <Formik
            initialValues={form}
            validationSchema={validationSchema}
            onSubmit={(data, {setSubmitting}) => {
              onSubmit(data, setSubmitting, setSuccess, setError);
            }}
        >
          {({isSubmitting}) => (
              <Form>
                <div className="formWrap">
                  <div className="textFieldWrapper">
                    <TextField
                        className="textField"
                        label="Hasło"
                        name="password"
                        type="password"
                    />
                  </div>
                  <div className="textFieldWrapper">
                    <TextField
                        className="textField"
                        label="Potwierdź Hasło"
                        name="rePassword"
                        type="password"
                    />
                  </div>
                </div>
                <button
                    className="btn btn-success mobileButton rightButton"
                    type="submit"
                    disabled={isSubmitting}
                >
                  Zmień
                </button>
              </Form>
          )}
        </Formik>
      </div>
  )
};

export default ChangePassword;
