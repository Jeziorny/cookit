import "./LogIn.scss";
import React, {useEffect} from "react";
import firebase from 'firebase/app';


/**
 * Component for logging and signing up, uses FirebaseUI
 */
const LogIn = () => {


  /**
   * Starts FirebaseUI with proper config object, where auth providers are set.
   */
  function startFirebaseUI() {
    // FirebaseUI config.
    const uiConfig = {
      // Url of endpoint to redirect in case of success
      signInSuccessUrl: "/fridge",
      signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        // TODO: config Facebook and Github authorization
        // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        // firebase.auth.GithubAuthProvider.PROVIDER_ID,
        // window.firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
      ]
    };

    // Initialize the FirebaseUI Widget using Firebase
    // (only if doesn't already exist, should be singleton - that's why we add it to window object)
    if (!window.ui) {
      const ui = new window.firebaseui.auth.AuthUI(firebase.auth());
      window.ui = ui;
    }
    // The start method will wait until the DOM is loaded.
    window.ui.start("#firebaseui-auth-container", uiConfig);
  }

  // useEffect is part of React Hooks - thanks to that we don't add the same script many times,
  // because the script is removed when component unmounts
  useEffect(() => {
    const script = document.createElement("script");
    script.src =
      "https://www.gstatic.com/firebasejs/ui/4.2.0/firebase-ui-auth__pl.js";
    script.async = true;
    script.onload = () => startFirebaseUI();
    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
      <div className="LogIn">
        <h1>Zaloguj lub Zarejestruj się</h1>
        <div className="mt-5" id="firebaseui-auth-container">
        </div>
      </div>
  );

};

export default LogIn;

