import React from "react";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import "./App.scss";

import Fridge from "../Fridge/Fridge";
import LogIn from "../LogIn/LogIn";
import Navbar from "../Navbar/Navbar";
import Favourites from "../Favourites/Favourites";
import Account from "../Account/Account";
import Recipe from "../Recipe/Recipe";
import RecipeFinder from "../RecipeFinder/RecipeFinder.js";
import { BreakpointProvider } from "react-socks";
import ShoppingScreen from "../ShoppingScreen/ShoppingScreen.js";

function App() {
  return (
    <div className="App">
      <BreakpointProvider>
        <BrowserRouter>
          <Navbar />
          <div className="container">
            <Switch>
              <Route exact path="/">
                <Redirect to="/fridge" />
              </Route>
              <Route path="/favourites" component={Favourites} />
              <Route path="/fridge" component={Fridge} />
              <Route path="/account" component={Account} />
              <Route path="/auth" component={LogIn} />
              <Route path="/shoppingList" component={ShoppingScreen} />
              <Route path="/recipes" component={RecipeFinder} />
              <Route path="/recipesList" component={RecipeFinder} />
              <Route path="/recipe/:id" component={Recipe} />
              <Route path="*">
                <Redirect to="/fridge" />
              </Route>
            </Switch>
          </div>
        </BrowserRouter>
      </BreakpointProvider>
    </div>
  );
}

export default App;
