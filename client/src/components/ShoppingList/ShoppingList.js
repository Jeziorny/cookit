import "./ShoppingList.scss"
import React, {useEffect, useState} from "react";
import Checkbox from "@material-ui/core/Checkbox";

const ShoppingList = ({ingredients, removeFromShoppingList}) => {

  const [ingr, setIngr] = useState(ingredients);

  const createIngCheck = () => {
    const arr = [];
    ingr.forEach(ingredient => {
      arr.push({
        [ingredient]: false
      })
    });
    return arr;
  };

  useEffect(() => {
    setState(createIngCheck());
  }, []);

  const [state, setState] = useState(createIngCheck());

  const handleChange = (name, key) => event => {
    setState({...state, [key]: {[name]: event.target.checked}});
  };

  const findBoughtProd = () => {
    const boughtProd = [];
    const values = Object.values(state);
    values.forEach(value => {
      const key = Object.keys(value);
      const val = Object.values(value);
      if (val[0]) {
        boughtProd.push(key[0]);
      }
    });
    setIngr(ingr.filter(el => !boughtProd.includes(el)));
    removeFromShoppingList(boughtProd);
  };

  return (
      <div className="ShoppingList">
        {ingr ?
            ingr.map((ingredient, key) => {
              return (
                  <label key={key}>
                    <Checkbox
                        checked={state[key][ingredient]}
                        onChange={handleChange(ingredient, key)}
                    />
                    {ingredient}
                  </label>
              )
            }) : null}
        <div className="wrapCenter">
          {ingr.length > 0 ?
              <button
                  className="btn btn-danger mt-4 mobileButton"
                  onClick={() => findBoughtProd()}
              >
                Usuń zaznaczone
              </button> :
              <h3>Nie dodałeś jeszcze żadnego produktu do listy.</h3>
          }
        </div>
      </div>
  )
};

export default ShoppingList;
