import React, { useState } from "react";
import "./Navbar.scss";
import firebase from "firebase/app";
import { NavLink } from "react-router-dom";
import { subscribeToAuthChange } from "../../utils/authUtils";
import Logo from "../Logo/Logo";

const logout = (event, user) => {
  event.persist();
  if (user) {
    firebase
      .auth()
      .signOut()
      .then(() => {});
  }
};

export default function Navbar() {
  const [user, setUser] = useState(null);

  subscribeToAuthChange(signedIn => {
    setUser(signedIn);
  }, signedOut => {
    setUser(signedOut);
  });


  return (
    <div className="Navbar">
      <div className="mobileLogo">
        <Logo />
      </div>
      <nav className="navbar navbar-expand navbar-dark">
        <ul className="navbar-nav col-12">
          <NavLink to="/fridge" className="navbar-brand mr-4 notMobile">
            <Logo />
          </NavLink>
          <li className="nav-item active">
            <NavLink to="/shoppingList" activeClassName="selected">
              <i className="fas fa-shopping-cart onMobile" />
              <span className="ml-1 notMobile">Lista zakupów</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/favourites" activeClassName="selected">
              <i className="fas fa-heart onMobile" />
              <span className="ml-1 notMobile">Ulubione</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/fridge" activeClassName="selected">
              <i className="fas fa-utensils onMobile" />
              <span className="ml-1 notMobile">Lodówka</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/recipes" activeClassName="selected">
              <i className="fas fa-scroll onMobile" />
              <span className="ml-1 notMobile">Przepisy</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/account" activeClassName="selected">
              <i className="fas fa-user onMobile" />
              <span className="ml-1 notMobile">Konto</span>
            </NavLink>
          </li>
          <li
            className="nav-item left ml-auto notMobile"
            onClick={event => logout(event, user)}
          >
            <NavLink to="/auth" activeClassName="selected">
              {user ? "Wyloguj" : "Zaloguj"}
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
}
