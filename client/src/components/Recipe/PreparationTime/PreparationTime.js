import React from "react";
import "./PreparationTime.scss";

export default function PreparationTime({ time, size }) {
  return (
    <div className="preparationTime" style={{ fontSize: size }}>
      <i className="far fa-clock mr-1" /> {time}
    </div>
  );
}
