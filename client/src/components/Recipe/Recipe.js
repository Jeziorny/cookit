import React, { useState, useEffect } from "react";
import IngredientsList from "../IngredientsList/IngredientsList";
import PreparationTime from "./PreparationTime/PreparationTime";
import firebase from "firebase";
import Loader from "../Loader/Loader";
import Img from "react-image";
import "./Recipe.scss";
import { subscribeToAuthChange } from "../../utils/authUtils";
import { firebasePaths, sessionStoragePaths } from "../../config/endPoints";

function Recipe({ match}) {
  const [recipe, setRecipe] = useState(null);
  const recipeID = match.params.id;
  const [user, setUser] = useState(null);
  let tempIngredients = [];
  const [availableIngredients, setAvailableIngredients] = useState(null)
  

  subscribeToAuthChange(
    signedIn => {
      setUser(signedIn);
    },
    signedOut => {
      setUser(signedOut);
    }
  );

  const fetch_recipe = async recipeID => {
    await firebase
      .database()
      .ref("recipes/")
      .once("value")
      .then(snap => {
        snap.forEach(child => {
          if (child.val().id_ == match.params.id) {
            setRecipe(child.val());
          }
        });
      });
  };

  const fetchAvailableIngredients = async () => {
    await firebase
      .database()
      .ref("user-ingredients/" + user.uid)
      .on("value", snapshot => {
        snapshot.forEach(userIngredient => {
          tempIngredients.push(userIngredient.val());
        });
      });
      
  };

  useEffect(() => {
    fetch_recipe(recipeID);
  }, [user]);

  const renderRecipe = () => {
    const description = (
      <div className="description">
        <br />
        <h4>Sposób przygotowania przepisu </h4>
        <br />
        <ol>
          {recipe.description.map((step, key) => {
            return <li key={key}> {step} </li>;
          })}
        </ol>
      </div>
    );

    return (
      <div>
        <div className="row">
          <div className="col-xl-12">
            <h2>{recipe.name}</h2>
            <br />
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-sm-12 col-md-4 col-xl-3">
            <PreparationTime time={recipe.prep_time} size={40} />
            <IngredientsList
              availableIngredients={tempIngredients}
              ingredients={recipe.ingredients}
              user={user}
            >
              {" "}
            </IngredientsList>
          </div>
          <div className="col-12 col-sm-12 col-md-8 col-xl-9">
            <Img src={recipe.img} width="100%" />
            <div className="col-xl-12 col-md-12 col-sm-12">{description}</div>
          </div>
        </div>
      </div>
    );
  };

  const renderLoader = () => {
    return <Loader />;
  };

  const renderRecipeViewUser = () => {
    fetchAvailableIngredients()
    return recipe ? renderRecipe() : renderLoader()
  } 

  const renderRecipeViewAnonymous = () => {
    getAnonymousIngredients(JSON.parse(sessionStorage.getItem(sessionStoragePaths.userIngredients)))
    return recipe ? renderRecipe() : renderLoader()
  }

  const getAnonymousIngredients = (object) => {
    for (var key in object) {
      tempIngredients.push(object[key])
    }
  }

  return (
    <div className="Recipe">{user ? renderRecipeViewUser() : renderRecipeViewAnonymous()}</div>
  );
}

export default Recipe;
