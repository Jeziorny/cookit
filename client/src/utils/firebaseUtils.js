import * as firebase from "firebase";

/**
 * Gets database reference to specific node
 *
 * @param path URI to specific database node
 * @returns {firebase.database.Reference} reference for given path
 */
export const getDatabaseRef = path => firebase.database().ref(path);

/**
 * Gets user id
 * @returns {string | null} user id for currently logged user (anonymously or not), null otherwise
 */
export const getUserId = () => {
  const currentUser = firebase.auth().currentUser;
  return currentUser ? currentUser.uid : null;
};