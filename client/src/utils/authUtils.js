import firebase from "firebase/app";

/**
 * Callback called when user has logged in non-anonymously (with email, Google etc.)
 * @callback SignedIn
 * @param {firebase.User} user has a lot of user properties (e.g. uid, email, displayName) and methods,
 *  @see {@link https://firebase.google.com/docs/reference/js/firebase.User}
 *
 */

/**
 * Callback called when user has logged out.
 * @callback SignedOut
 */

/**
 * Every call of this method subscribes to "authorization status change" event.
 * Event is fired in two cases - when user signs in or signs out.
 * @param {SignedIn} signedInCallback called when user signs in non-anonymously
 * @param {SignedOut} signedOutCallback called when user signs out
 * @return {firebase.Unsubscribe} function can be called to unsubscribe
 */
export const subscribeToAuthChange = (signedInCallback, signedOutCallback) =>
  firebase.auth().onAuthStateChanged(user => {
    if (user) {
      sessionStorage.clear()
      signedInCallback(user);
    } else {
      signedOutCallback();
    }
  });
