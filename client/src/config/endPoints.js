export const firebasePaths = {
  userIngredients: 'user-ingredients/',
  recipes: 'recipes/',
  ingredientsWithRelatedRecipes: 'ing_to_recp_id/',
  favourites: 'favourites/'
};

export const sessionStoragePaths = {
  userIngredients: 'user-ingredients'
};