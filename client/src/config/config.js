/**
 * Unique configuration object for Firebase (got from Firebase console)
 */
export const firebaseConfig = {
  apiKey: "AIzaSyAmU_-rTbLywXTmGO6h4xzb8nymfzL2sKs",
  authDomain: "cookit-eab9b.firebaseapp.com",
  databaseURL: "https://cookit-eab9b.firebaseio.com",
  projectId: "cookit-eab9b",
  storageBucket: "cookit-eab9b.appspot.com",
  messagingSenderId: "938603358128",
  appId: "1:938603358128:web:bc0e5cba54164bf782d875",
  measurementId: "G-DLPFQG2WVX"
};
