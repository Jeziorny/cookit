const useDisplayName = displayName => {
  const [firstName, lastName] = displayName.split(" ");

  return [firstName, lastName];
};

export default useDisplayName;
